<?php
function jskitcomments_views_data_alter(&$data) {
  $data['node']['jskitcomments_count'] = array(
    'field' => array(
      'title' => t('JS-Kit Echo Comments Count'),
      'help' => t('The number of JS-Kit Echo Comments for the node.'),
      'handler' => 'views_handler_field_jskitcomments_count',
    ),
  );
  $data['node']['jskitcomments_widget'] = array(
    'field' => array(
      'title' => t('JS-Kit Echo Comments Widget'),
      'help' => t('JS-Kit Echo Comments Widget for the node.'),
      'handler' => 'views_handler_field_jskitcomments_widget',
    ),
  );
}
